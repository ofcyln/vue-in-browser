Vue.component('user-name', {
  props: {
    name: String
  },
  template: '<p>Hi {{ name }}</p>'
})

Vue.component('vueselect', {
  props: {
    options: Array
  },
  template: `
    <select @change="handleChange">
      <option
        v-for="(index, key) in options"
        :key="key"
      >
        {{index}}
      </option>
    </select>
  `,
  methods: {
    handleChange({ target: { value } }) {
      console.log(value)
    }
  }
})

new Vue({
  el: "#app"
})